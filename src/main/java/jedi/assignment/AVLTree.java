package jedi.assignment;

public class AVLTree {
    Node root;
    class Node{
        int item,height;
        Node left, right;

        Node(int d){
            this.item = d;
            this.height =1;
        }

    }

    int max(int a,int b) {
        return	a>b ?a : b ;
    }

    int height(Node node) {
        return (node == null) ? 0: node.height;

    }

    int updateHeight(Node node) {
        return max(height(node.left),height(node.right))+1;
    }

    int getBalance(Node node) {
        return (node == null) ? 0 : height(node.left) - height(node.right);
    }

    Node rightRotate(Node y) {
        Node x = y.left;
        Node z = x.right;

        x.right = y;
        y.left = z;

        y.height =updateHeight(y);
        x.height = updateHeight(x);

        return x;
    }

    Node leftRotate(Node y) {
        Node x = y.right;
        Node xL = x.left;
        x.left = y;
        y.right = xL;

        x.height =updateHeight(x);
        y.height = updateHeight(y);

        return x;

    }


    Node minValueNode(Node node) {
        Node current = node;
        while(current.left !=null )
            current = current.left;

        return current;

    }


    Node insert(Node node, int item) {
        if(node == null) return new Node(item);
        if(item < node.item)
            node.left = insert(node.left,item);
        else if(item > node.item)
            node.right = insert(node.right,item);
        else
            return node;
        node.height = updateHeight(node);
        int balance = getBalance(node);
        if( balance > 1 ) {
            if(item < node.left.item)return rightRotate(node);
            else if(item > node.left.item) {
                node.left = leftRotate(node.left);
                return rightRotate(node);
            }
        }

        if(balance < -1 ) {
            if( item > node.right.item)return leftRotate(node);
            else if(item < node.right.item) {
                node.right = rightRotate(node.right);
                return leftRotate(node);
            }
        }

        return node;
    }
    public Node search(int key) {
        Node current = root;
        while(current != null) {
            if(current.item == key) {
                break;
            }
            current =  current.item >key ? current.left : current.right;
        }

        return current;
    }

    Node delete(Node root, int key) {
        if(root == null) return root;
        if(key< root.item)
            root.left = delete(root.left,key);
        else if(key > root.item)
            root.right = delete(root.right,key);
        else {
            if((root.left == null) || (root.right == null)) {
                Node temp = null;
                temp = root.left == null ?root.right : root.left;
                root = temp;
            }else {
                Node temp = minValueNode(root.right);
                root.item = temp.item;
                root.right = delete(root.right, temp.item);
            }
        }

        if(root == null) return root;

        root.height = updateHeight(root);

        int balance = getBalance(root);

        if(balance > 1 ) {
            if(getBalance(root.left) >=0)return rightRotate(root);
            else {
                root.left  = leftRotate(root.left);
                return rightRotate(root);
            }

        }

        if(balance < -1 ) {
            if( getBalance(root.right)<=0)return leftRotate(root);
            else {
                root.right = rightRotate(root.right);
                return leftRotate(root);
            }
        }
        return root;
    }



    void preOrder(Node root) {
        if(root != null) {
            System.out.print(root.item + " ");
            preOrder(root.left);
            preOrder(root.right);
        }
    }
    void inOrder(Node root) {
        if(root != null) {
            inOrder(root.left);
            System.out.print(root.item + " ");
            inOrder(root.right);
        }
    }
    void postOrder(Node root) {
        if (root != null) {
            postOrder(root.left);
            postOrder(root.right);
            System.out.print(root.item + " ");
        }
    }

    public static void main(String args[]) {
        AVLTree tree = new AVLTree();

        tree.root = tree.insert(tree.root, 9);
        tree.root = tree.insert(tree.root, 5);
        tree.root = tree.insert(tree.root, 10);
        tree.root = tree.insert(tree.root, 0);
        tree.root = tree.insert(tree.root, 6);
        tree.root = tree.insert(tree.root, 11);
        tree.root = tree.insert(tree.root, -1);
        tree.root = tree.insert(tree.root, 1);
        tree.root = tree.insert(tree.root, 2);
        System.out.println("preorder  traversal of constructing tree is :");
        tree.preOrder(tree.root);
        tree.root = tree.delete(tree.root, 10);
        System.out.println("preorder traversal after deletion of 10");
        System.out.println();

        tree.preOrder(tree.root);
        System.out.println();
        tree.inOrder(tree.root);
        System.out.println();
        tree.postOrder(tree.root);

    }
}

