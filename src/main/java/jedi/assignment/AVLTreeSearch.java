package jedi.assignment;

public class AVLTreeSearch {
    Node root;
    class Node{
        int item,height;
        Node left, right;

        Node(int d){
            this.item = d;
            this.height =1;
        }

    }

    int max(int a,int b) {
        return	a>b ?a : b ;
    }

    int height(Node node) {
        return (node == null) ? 0: node.height;

    }

    int updateHeight(Node node) {
        return max(height(node.left),height(node.right))+1;
    }

    int getBalance(Node node) {
        return (node == null) ? 0 : height(node.left) - height(node.right);
    }

    Node rightRotate(Node y) {
        Node x = y.left;
        Node z = x.right;

        x.right = y;
        y.left = z;

        y.height =updateHeight(y);
        x.height = updateHeight(x);

        return x;
    }

    Node leftRotate(Node y) {
        Node x = y.right;
        Node xL = x.left;
        x.left = y;
        y.right = xL;

        x.height =updateHeight(x);
        y.height = updateHeight(y);

        return x;

    }



    Node insert(Node node, int item) {
        if(node == null) return new Node(item);
        if(item < node.item)
            node.left = insert(node.left,item);
        else if(item > node.item)
            node.right = insert(node.right,item);
        else
            return node;
        node.height = updateHeight(node);
        int balance = getBalance(node);
        if( balance > 1 ) {
            if(item < node.left.item)return rightRotate(node);
            else if(item > node.left.item) {
                node.left = leftRotate(node.left);
                return rightRotate(node);
            }
        }

        if(balance < -1 ) {
            if( item > node.right.item)return leftRotate(node);
            else if(item < node.right.item) {
                node.right = rightRotate(node.right);
                return leftRotate(node);
            }
        }

        return node;
    }
    public Node search(int key) {
        Node current = root;
        while(current != null) {
            if(current.item == key) {
                break;
            }
            current =  current.item >key ? current.left : current.right;
        }

        return current;
    }






    public static void main(String args[]) {
        AVLTreeSearch tree = new AVLTreeSearch();

        tree.root = tree.insert(tree.root, 5);
        tree.root = tree.insert(tree.root, 11);
        tree.root = tree.insert(tree.root, 9);
        tree.root = tree.insert(tree.root, -1);
        tree.root = tree.insert(tree.root, 2);
        tree.root = tree.insert(tree.root, 6);
        tree.root = tree.insert(tree.root, 1);
        tree.root = tree.insert(tree.root, 10);
        tree.root = tree.insert(tree.root, 0);


        Node current = tree.search(5);
        System.out.println(tree.height(current.right)+1);

    }
}
